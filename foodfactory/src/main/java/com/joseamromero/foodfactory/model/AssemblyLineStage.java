package com.joseamromero.foodfactory.model;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;

public interface AssemblyLineStage {
	/**
	 * Put the specified product to the assembly line to continue in the next stage.
	 * 
	 * @param product
	 */

	void putAfter(Product product);

	/**
	 * Takes the next product available from the assembly line.
	 * 
	 * @return
	 */
	void take();
	

}

