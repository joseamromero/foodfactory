package com.joseamromero.foodfactory.event;

import org.springframework.context.ApplicationEvent;

import com.joseamromero.foodfactory.model.AssemblyLineStage;
import com.joseamromero.foodfactory.model.Product;

public class AssemblyLineEvent extends ApplicationEvent {
	
	private boolean isBaked;
	private AssemblyLineStage AssemblyLineStage;
	private Product product;

	public AssemblyLineEvent(Object source,AssemblyLineStage stage, Product product) {
		super(source);
		this.AssemblyLineStage = stage;		
		
		
	}
	
	public boolean endBake() {
		return isBaked;
	}


}
