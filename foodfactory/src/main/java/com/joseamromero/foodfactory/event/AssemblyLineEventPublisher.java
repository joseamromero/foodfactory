package com.joseamromero.foodfactory.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
class Publisher {

	private final ApplicationEventPublisher publisher;

	Publisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

}