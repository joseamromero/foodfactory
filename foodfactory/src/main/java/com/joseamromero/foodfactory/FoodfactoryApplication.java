package com.joseamromero.foodfactory;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.joseamromero.foodfactory.model.AssemblyLineStage;
import com.joseamromero.foodfactory.model.Product;
import com.joseamromero.foodfactory.service.GenericProduct;

@SpringBootApplication
public class FoodfactoryApplication implements CommandLineRunner {

	@Resource
	AssemblyLineStage aseemblyLineStage;

	private static final Logger logger = LoggerFactory.getLogger(FoodfactoryApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(FoodfactoryApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		

		Product productA = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productB = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productC = new GenericProduct(generateRandomDuration(), generateRandomSize());

		aseemblyLineStage.putAfter(productA);
		aseemblyLineStage.putAfter(productB);
		aseemblyLineStage.putAfter(productC);
		aseemblyLineStage.putAfter(productA);
		aseemblyLineStage.putAfter(productB);
		aseemblyLineStage.putAfter(productC);

	}

	@Bean(name = "assemblyLineTaskExecutor")
	public Executor assemblyLineTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(4);
		executor.setMaxPoolSize(4);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("AseemblyLineTE-");

		executor.initialize();
		return executor;
	}

	@Bean(name = "ovenTaskExecutor")
	public Executor ovenTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(4);
		executor.setQueueCapacity(0);
		executor.setThreadNamePrefix("ovenTE-");

		executor.initialize();
		return executor;
	}

	@Bean(name = "storeTaskExecutor")
	public Executor storeTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("storeTE-");

		executor.initialize();
		return executor;
	}

	private Duration generateRandomDuration() {
		return Duration.ofMillis(ThreadLocalRandom.current().nextInt(2000, 7000));
	}

	private double generateRandomSize() {
		return ThreadLocalRandom.current().nextInt(500, 1000);
	}

}
