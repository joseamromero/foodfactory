package com.joseamromero.foodfactory.controller;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joseamromero.foodfactory.model.AssemblyLineStage;
import com.joseamromero.foodfactory.model.Product;
import com.joseamromero.foodfactory.service.AseemblyLineStageService;
import com.joseamromero.foodfactory.service.GenericProduct;

@RestController
public class FoodFactoryController {

	private static final Logger logger = LoggerFactory.getLogger(FoodFactoryController.class);
	@Autowired
	AssemblyLineStage assemblylineService;

	@RequestMapping(value = "assemblyLine/put/mock")
	public void putInOven() throws InterruptedException {
		Product productA = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productB = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productC = new GenericProduct(generateRandomDuration(), generateRandomSize());

		assemblylineService.putAfter(productA);

		assemblylineService.putAfter(productB);

		assemblylineService.putAfter(productC);

	}

	private Duration generateRandomDuration() {
		return Duration.ofMillis(ThreadLocalRandom.current().nextInt(2000, 7000));
	}

	private double generateRandomSize() {
		return ThreadLocalRandom.current().nextInt(100, 500);
	}

}
