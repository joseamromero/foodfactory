package com.joseamromero.foodfactory.service;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.joseamromero.foodfactory.exception.CapacityExceededException;
import com.joseamromero.foodfactory.model.Product;
import com.joseamromero.foodfactory.model.Store;

@Service
public class StoreService implements Store {
	private double size = 2000;

	private static final Logger logger = LoggerFactory.getLogger(OvenService.class);

	@Async("storeTaskExecutor")
	public void put(Product product) throws CapacityExceededException {
		logger.info("###Entering StoreService with Thread id: " + Thread.currentThread().getId());

		if (product.size() < this.size) {
			this.size -= product.size();
		} else {
			throw new CapacityExceededException("¡HALT THE LINE!");
		}

	}

	@Override
	public Product take() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void take(Product product) {
		// TODO Auto-generated method stub

	}

}
