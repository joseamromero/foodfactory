package com.joseamromero.foodfactory.service;

import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.joseamromero.foodfactory.model.AssemblyLineStage;
import com.joseamromero.foodfactory.model.Oven;
import com.joseamromero.foodfactory.model.Product;
import com.joseamromero.foodfactory.model.Store;

@Service
public class AseemblyLineStageService implements AssemblyLineStage {
	private static final Logger logger = LoggerFactory.getLogger(AseemblyLineStageService.class);

	@Autowired
	private Oven oven;
	@Autowired
	private Store store;

	@Async("assemblyLineTaskExecutor")
	public void putAfter(Product product) {

		CompletableFuture.supplyAsync(() -> {

			if (oven.size() > product.size()) {
				logger.info("###Entering AssamblyLine-Oven with Thread id: " + Thread.currentThread().getId());
				oven.put(product);
			} else {
				logger.info("###Entering AssamblyLine-Store with Thread id: " + Thread.currentThread().getId());
				store.put(product);
			}
			return 0;
		}).exceptionally(CapacityExceededException -> {
			logger.info("###In Exception: : " + CapacityExceededException);
			store.put(product);
			return -1;
		}).exceptionally(CapacityExceededException -> {
			logger.info("###In Exception: : " + CapacityExceededException);

			return -1;
		});

	}
	//TODO @EventListener reconocer la finalizacion del horneado y sacar el producto
	@Async("assemblyLineTaskExecutor")
	public void take() {
		logger.info(
				"###Entering AssamblyLine-take, product finished!  with Thread id: " + Thread.currentThread().getId());

	}

}
