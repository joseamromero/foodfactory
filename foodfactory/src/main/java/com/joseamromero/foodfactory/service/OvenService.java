package com.joseamromero.foodfactory.service;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.joseamromero.foodfactory.exception.CapacityExceededException;
import com.joseamromero.foodfactory.model.Oven;
import com.joseamromero.foodfactory.model.Product;

@Service
public class OvenService implements Oven {
	private static final Logger logger = LoggerFactory.getLogger(OvenService.class);

	private double size = 3000;

	public OvenService() {

	}

	@Override
	public double size() {
		return size;
	}

	@Async("ovenTaskExecutor")
	public void put(Product product) throws CapacityExceededException {

		bake(product);

	}

	private void bake(Product product) {
		logger.info("###Baking Thread id: " + Thread.currentThread().getId());

		if (this.size > product.size()) {
			this.size -= product.size();
			logger.info("###Current Capacity of the oven : " + this.size);
			try {
				Thread.sleep(product.cookTime().toMillis());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.info("##It's Baked! " + Thread.currentThread().getId());
		} else {
			throw new CapacityExceededException("Horno lleno, enviar al store");
		}

	}

	@Override
	public void take(Product product) {
		logger.info("Horneado, sacando " + product.toString() + " del horno " + this.toString());
		this.size += product.size();

	}

	@Override
	public void turnOn() {
		// TODO Auto-generated method stub

	}

	@Override
	public void turnOn(Duration duration) {
		// TODO Auto-generated method stub

	}

	@Override
	public void turnOff() {
		// TODO Auto-generated method stub

	}

}
