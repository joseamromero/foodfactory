package com.joseamromero.foodfactory.service;

import java.time.Duration;

import com.joseamromero.foodfactory.model.Product;

public class ProductImpl implements Product {

	private double size;
	private Duration duration;

	public ProductImpl(double size, Duration duration) {
		super();
		this.size = size;
		this.duration = duration;
	}

	@Override
	public double size() {

		return this.size;
	}

	@Override
	public Duration cookTime() {
		
		return this.duration;
	}

}
