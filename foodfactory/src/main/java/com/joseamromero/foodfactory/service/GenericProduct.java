package com.joseamromero.foodfactory.service;

import java.time.Duration;

import com.joseamromero.foodfactory.model.Product;

public class GenericProduct implements Product {

	private Duration duration;
	private double size;

	public GenericProduct(Duration duration, double size) {
		super();
		this.duration = duration;
		this.size = size;
	}

	@Override
	public double size() {
		return this.size;
	}

	@Override
	public Duration cookTime() {
		return this.duration;
	}

}
