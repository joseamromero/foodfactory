package com.joseamromero.foodfactory;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.joseamromero.foodfactory.model.AssemblyLineStage;
import com.joseamromero.foodfactory.model.Product;
import com.joseamromero.foodfactory.service.GenericProduct;

@SpringBootTest
class FoodfactoryApplicationTests {

	@Autowired
	AssemblyLineStage assemblylineService;

	@Test
	void contextLoads() {
	}

	@Test
	public void TestGenerico() {
		Product productA = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productB = new GenericProduct(generateRandomDuration(), generateRandomSize());
		Product productC = new GenericProduct(generateRandomDuration(), generateRandomSize());

		assemblylineService.putAfter(productA);

		assemblylineService.putAfter(productB);

		assemblylineService.putAfter(productC);
		assemblylineService.putAfter(productA);

		assemblylineService.putAfter(productB);

		assemblylineService.putAfter(productC);
		
		assemblylineService.putAfter(productC);
		assemblylineService.putAfter(productA);

		assemblylineService.putAfter(productB);

		assemblylineService.putAfter(productC);
	}

	private Duration generateRandomDuration() {
		return Duration.ofMillis(ThreadLocalRandom.current().nextInt(2000, 7000));
	}

	private double generateRandomSize() {
		return ThreadLocalRandom.current().nextInt(100, 700);
	}

}
